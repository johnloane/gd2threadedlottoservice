package Server;

import Core.LottoServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class LottoStreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        try
        {
            //Set up the listening socket
            listeningSocket = new ServerSocket(LottoServiceDetails.LISTENING_PORT);

            //Set up a ThreadGroup to manage all of the clients together
            ThreadGroup clientGroup = new ThreadGroup("Client threads");

            clientGroup.setMaxPriority(Thread.currentThread().getPriority()-1);

            //Do the main logic of the server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning)
            {
                //Accept incoming connections
                dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("The server has now accepted " + threadCount + " clients");

                LottoServiceThread newClient = new LottoServiceThread(clientGroup, dataSocket.getInetAddress()+"", dataSocket, threadCount);
                newClient.start();
            }
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect " + ioe.getMessage());
                System.exit(1);
            }
        }
    }
}
