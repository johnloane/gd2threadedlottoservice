package Server;

import Core.LottoServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class LottoServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;
    private static final int MAX_NUMBER_OF_NUMBERS = 7;

    public LottoServiceThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);

        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader((this.dataSocket.getInputStream())));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(LottoServiceDetails.CLOSE))
            {
                response = null;

                //take input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                //Break the incomingMessage into components
                String[] components = incomingMessage.split(LottoServiceDetails.COMMAND_SEPARATOR);

                if(components[0].equals(LottoServiceDetails.GENERATE))
                {
                    if(components.length > 1)
                    {
                        response = generateResponse(Integer.parseInt(components[1]));
                    }
                }
                else
                {
                    response = LottoServiceDetails.UNRECOGNISED;
                }

                System.out.println("Sending response: " + response);
                output.println(response);
            }
        }
        catch(NoSuchElementException nse)
        {
            System.out.println(nse.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("\n Closing connection with client #" + number + "...");
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    public static String generateResponse(int numberOfNumbers)
    {
        if(numberOfNumbers > MAX_NUMBER_OF_NUMBERS)
        {
            return "Number is too big";
        }

        StringBuilder message = new StringBuilder(LottoServiceDetails.GENERATED);
        message.append(LottoServiceDetails.COMMAND_SEPARATOR);
        Random random = new Random();
        int[] nums = random.ints(1, 50).distinct().limit(numberOfNumbers).sorted().toArray();
        for(int i=0; i < numberOfNumbers; ++i)
        {
            message.append(Integer.toString(nums[i]));
            message.append(LottoServiceDetails.COMMAND_SEPARATOR);
        }
        return message.toString();
    }


}
