package Client;

import Core.LottoServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class LottoServiceClient
{
    public static void main(String[] args)
    {
        Socket dataSocket = null;
        try
        {
            //Step 1: Establish a channel of communication
            dataSocket = new Socket("localhost", LottoServiceDetails.LISTENING_PORT);

            //Step 2: Build the input and output objects
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            while(!message.equals(LottoServiceDetails.CLOSE))
            {
                displayMenu();
                int choice = getNumber(keyboard);
                String response = "";

                if(choice > 0 && choice < 3)
                {
                    switch(choice)
                    {
                        case 1:
                            message = generateNumberRequest(keyboard);

                            //Send the message
                            output.println(message);
                            output.flush();

                            //Get the response from the server
                            response = input.nextLine();
                            if(response.equals("Number is too big"))
                            {
                                System.out.println("You are looking for more than 7 numbers. This service only returns 7 or less numbers");
                            }
                            else
                            {
                                processNumbers(response);
                            }
                            break;

                        case 2:
                            message = LottoServiceDetails.CLOSE;

                            output.println(message);
                            output.flush();
                            break;
                    }
                    if(response.equals(LottoServiceDetails.UNRECOGNISED))
                    {
                        System.out.println("Sorry, that command is not recognised");
                    }
                }
                else
                {
                    System.out.println("Please select a number from the menu 1 or 2");
                }
            }
            System.out.println("Thank you for using the Lotto Stream Service");
        }
        catch(UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    public static void displayMenu()
    {
        System.out.println("1) To generate lotto numbers");
        System.out.println("2) To close the client and end the session");
    }

    public static int getNumber(Scanner keyboard)
    {
        boolean numberEntered = false;
        int number = 0;
        while(!numberEntered)
        {
            try
            {
                number = keyboard.nextInt();
                numberEntered = true;
            }
            catch(InputMismatchException ime)
            {
                System.out.println("Please enter a number");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return number;
    }

    public static String generateNumberRequest(Scanner keyboard)
    {
        StringBuffer message = new StringBuffer(LottoServiceDetails.GENERATE);
        message.append(LottoServiceDetails.COMMAND_SEPARATOR);

        System.out.println("Please enter the number of lotto numbers you want. Must be less than 7: ");
        int number = getNumber(keyboard);

        message.append(number);

        return message.toString();
    }

    public static void processNumbers(String numbers)
    {
        String[] components = numbers.split(LottoServiceDetails.COMMAND_SEPARATOR);
        ArrayList<Integer> lottoNumbers = new ArrayList<>();

        System.out.println(numbers);
        if(components.length > 1)
        {
            for(int i=1; i <  components.length; ++i)
            {
                lottoNumbers.add(Integer.parseInt(components[i]));
            }
        }
        printLottoNumbers(lottoNumbers);
        System.out.println();
    }

    public static void printLottoNumbers(ArrayList<Integer> lottoNumbers)
    {
        for(Integer number : lottoNumbers)
        {
            System.out.print(number + " ");
        }
    }
}
