package Core;

public class LottoServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    //Breaking characters
    public static final String COMMAND_SEPARATOR = "%";

    //Command strings
    public static final String GENERATE = "GENERATE";
    public static final String CLOSE = "***CLOSE***";

    //Response strings
    public static final String GENERATED = "GENERATED";
    public static final String SESSION_TERMINATED = "GOODBYE";
    public static final String UNRECOGNISED = "UNKNOWN_COMMAND";
}
